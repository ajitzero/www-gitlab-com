---
layout: handbook-page-toc
title: "Commissions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Quotas Overview

The quota coverage types are:
1. **Native Quota Req (NQR)** - individual owns direct quota for their assigned territory.
2. **Overlay Quota Rep (OQR)** - individual supports an assigned territory that is currently owned by a NQR.

Quotas may be adjusted based on geographic region and other factors.

### Quota Components

The following components are typical for most comp plans:
1. IACV: regional, territory, segment, or industry based
2. Professional Services: for native-quota carrying reps, the annual target is $50,000. For overlay quota reps, the target can be based on the reps who you support or roll up to you.
3. Professional Services Margins: this is typical for Implementation Engineers

For a list of the current comp plans, please review this Google Slides [presentation](https://docs.google.com/presentation/d/1S0X4AOq6DdP63T9AWDBhzPLuHU6jHJMlB4YHzMxhfjA/edit#slide=id.g33c9f07e39_1_37)

### Sales Rep IACV Quota Ramp and Seasonality Assumptions

For all native-quota carrying salespeople, we assume around an eleven (11) month ramp before a rep is fully productive.  We have the following ramp up schedule and measure performance based on this schedule:

* First Quarter Hired: 0% contribution of quarterly target
* Second Quarter Hired: 30% contribution of quarterly target
* Third Quarter Hired: 65% contribution of quarterly target
* Fourth Quarter Hired: 90% contribution of quarterly target

We also factor in seasonality into our calculations. We expect most of our business to close in the second half of the year. Our seasonality assumptions are as follows for the private sector:

* First Fiscal Quarter: 17%
* Second Fiscal Quarter: 23%
* Third Fiscal Quarter: 28%
* Fourth Fiscal Quarter: 32%

Given the fiscal period for public sector, our seasonality assumptions differ:

* First Fiscal Quarter: 15%
* Second Fiscal Quarter: 20%
* Third Fiscal Quarter: 55%
* Fourth Fiscal Quarter: 10%

In order to establish the quota for a given representative, the fully ramped quota and the start date are required. Once obtained, Sales Operations can proivde a prorated quota verbally to the Area Sales Manager, Regional Director, or Vice President, then deliver a Participant Schedule to the representative for acknowledgement of their plan and quota.

### Quotas Proration
1. Native Quota-Carrying Reps: prorated quotas are based on the seasonality assumptions as demonstrated above.
2. Overlay Quota Reps: proration is based off the number of days employed in the given period. Your prorated quota would be calculated as follows: (Quota / (Period End Date - Period Start Date)) * (Period End Date - Hire Date). For example, if the regional quota is $10,000,000 for the year and the start date is 4/1/2019, your quota will be calculated as ($10,000,000 / (2020-01-31 - 2019-02-01)) * (2020-01-31 - 2019-04-01) = $8,379,120.88.

## Commissions Overview
Most plans are paid monthly; however there are some plans that are paid on a quarterly basis. Most commissions plans are calculated in Xactly. 

### Commission Rates
You can find the definitions of **Base Commission Rate (BCR)** and **Super Commission Rates (SCR)** in the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/#definitions) page under the Definitions section.

### Commissions Rules
The following rules will apply for the various roles within the organization:

| **Role** | **Rules**  | **Explanation** |
| ------ | ------ | ------ |
| Strategic Account Leader, <br>Mid Market Account Executive | Is the Opportunity Owner | Opportunity owner will receive the commission |
| SMB Customer Advocate | Sales Segment is SMB or Unknown | The Ultimate Parent Segment is either SMB |
| Area Sales Manager | Opportunity Owner is subordinate AND<br>Account Owner is on Regional Team | If the Opportunity owner is a subordinate AND the Account Owner is on the ASM's regional team, the ASM will receive credit |
| Regional Director | Account Owner is your subordinate | The opportunity owner may be on a different regional team, but if the Account Owner is on the RD's team, the RD will receive credit. It is possible to override if the RD of the opportunity owner requests credit for the deal |
| Vice President (Sales) | Account Owner is your subordinate | The opportunity owner may be on a different Regional team, but if the Account Owner is on the VP's team, the VP will receive credit |
| Inside Sales Representatives (Public Sector) | Sub-Industry | The opportunity owner may be anyone on the PubSec team so long as the sub-industry is the one supported by the ISR |
| Solutions Architects | Opportunity Owner is on your regional team | The opportunity owner must be on the SA's team for them to receive credit |
| Technical Account Manager | Opportunity Owner is on your regional team | The opportunity owner must be on the TAM's team for them to receive credit |

### Hold and Releases
Commissions are paid at the time of collection only when the full invoice amount is received. If the account is not invoiced in full no commissions will be paid.  These commissions will be held in the Commission system and will be released by the billing team at the time of collection. In case of special situation where the commissions need to be released even though not invoiced in full we would need written approval from the CRO and CFO to release those commissions.



### Opportunity Splits
Coming Soon

## Participant Schedules
Within the first few weeks of your employment, you should receive your participant schedule. This document will provide a high level overview of your plan components, territory, plan start and end dates, base and super (if applicable) commission rates, prorated quotas, and variable payouts per component.

## Sales Representation Letter 

### Introduction

As part of SOX 404 control environment all quota carrying reps need to certify if there were any deviations to the handbook policy with respect to quote approval and contract management process. Reps should read and acknowledge the letter and report any exceptions.

### Goals of certification

The main aim of this rep certification letter is to identify: 

* Any form of communication with a customer that would modify or supersede the terms of an existing subscription and / or a subscription agreement that has not yet been documented or signed. 
  
    These arrangements typically result from efforts to: remediate a performance issue, secure future business, obtain a customer reference, retain or improve customer satisfaction, avoid a legal matter or close a deal. Examples of benefits offered to customers could include the following: free or discounted products or services, write-off of a previously recorded account receivable balance, ability of a distributor to re-sell a license to non-intended end-users, extended warranty rights, offer to return products not sold for full refund outside of policies, payment terms that deviate from GitLab’s standard practice or cash refunds.

### Sales Representation Letter Certifies:

1.  All written or unwritten commitments to customers are documented in the Agreement, Order Form, PO or other written agreement signed by the Customer and authorized signatory at Gitlab.  If commitments are not included in customer agreements then they have been documented in a GitLab issue in which the CEO, CFO or CRO has been tagged.

2. All customer contract files (including purchase orders, contracts, letter agreements, sales offers, amendments, and any other correspondence) are complete, properly signed by each party, and the appropriate contract record in Salesforce is filled in so it is easily accessible.

3. All indirect sales (e.g. those through a partner/distributor/ reseller) are supported by a bona fide end-user purchase order or contract.

4. All purchase orders and/or contracts with customers were fully executed by an appropriate customer employee and authorized GitLab employee, if required by local business practice, on or before the delivery date of such contracts.

5. All side arrangements (if any) with customers have been approved by a GitLab executive with authority to make such arrangements and has subsequently been disclosed to the GitLab Revenue Recognition Team (“Revenue Team” controller, senior technical accounting manager, the Principal Accounting Officer, or CFO) in writing.

6. The Company’s customers have not a) been granted any rights to return products;               b) receive credits for products or services delivered or receive additional products or services that are not specifically included in the agreements or amendments with such customers, or c) transfer of licenses or delivery of the software to an unauthorized end-user. 

7. We did not communicate, directly or indirectly, to any customer that they are free to activate and use the license subscription prior to the subscription start date agreed in the executed order form. Further, we have communicated any knowledge of such unauthorized use to the Finance team.

8. All agreements with unfulfilled performance guarantees or remaining acceptance provisions have been disclosed in the customer agreement or amendments and to the Revenue Team.

9. Multiple agreements with a single customer meeting any of the following criteria have been disclosed to the Revenue Team: agreements were executed within a short time frame (i.e. within 3 months) of each other, the contract terms are interrelated (i.e. payment terms under one agreement are linked to the performance of another), negotiations of the contracts were conducted jointly.

10. The Company has not granted payment terms that deviate from GitLab’s standard practice under any customer sales agreements that are not specifically disclosed in the customer agreement or amendments.

11. All customer arrangements for which payment is contingent on the customer’s obtaining financing from an outside funding source have been disclosed in the customer agreement.

12. There are no known credit memos to be recorded/issued subsequent to the end of the quarter noted above that relate to products or services delivered during the quarter or prior, which would reverse revenue recorded for that arrangement equal to or in excess of 5% of the deal value.

13. Please confirm if there were any changes to quotes after approval or if quotes are sent to a customer and/or not reviewed by Sales operations or Deal desk , please disclose.

14. I personally questioned each of my direct reports (if any) with respect to the representations noted above and am not aware of any deviations not already disclosed.

15. During the Quarter, I have not engaged in any activity, nor have I become aware of any other GitLab employee or employee of any GitLab subsidiary engaging in any activity, which violates the Company’s Code of Business Conduct (https://about.gitlab.com/handbook/people-operations/code-of-conduct/), including any activity in violation of the Foreign Corrupt Practices Act - which prohibits any payments to foreign (in jurisdictions outside of the United States) officials for the purpose of obtaining or keeping business. 

16. Nothing has come to my attention from previous quarter sales activities that have changed or been disclosed to me that I was not aware of previously, but now am aware of.

### Sales Rep Certification Process

Rep letters will be sent quarterly by the Internal Audit Team. Reps are required to acknowledge the certification letter.

### Sales Rep Certification Process Timeline

| **Activity** | **Timeline** |  
| ------ | ------ |  
| Roll out certificates by Internal Audit | 25th of the last month of the quarter |  
| Receive responses by | 8th of the subsequent month after the quarter end |  
| Internal Audit to collate all the information and communicate to Principal Accounting Officer | 9th of the subsequent month after the quarter end |  
| Principal Accounting Officer and Chief Financial Officer to review  the responses and  to communicate to audit committee | 12th of the subsequent month after the quarter end |  

* If any of the timelines above fall under a public holiday then the previous working day will be a due date for the respective activity.
