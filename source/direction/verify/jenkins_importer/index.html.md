---
layout: markdown_page
title: "Category Direction - Jenkins Importer"
---

- TOC
{:toc}

## Jenkins Importer

The Jenkins Importer category represents a collection of tools and documentation to help you migrate away from your existing Jenkins implementation and easily move to GitLab CI/CD. There can be a lot of complexity in the details when it comes to a large, enterprise Jenkins installation that uses a lot of different maintained (or even non-maintained) plugins, but our goal is to make 80% of the automatable tasks easy and then offer Professional Services as needed for the remaining 20%.

People ask for a Jenkins importer often: they are bought into the GitLab concept, they can quickly import their projects from GitHub/Bitbucket and the next logical question is "okay how about Jenkins?" Customers are fearful of the amount of time it's going to take to "rebuild" their CI pipelines in another system. This comes from the years of effort they've had to put into building and maintaining Jenkins and their pipelines, and they are fearful of going down that road again. At least for the moment, many enterprises do not yet see Auto DevOps as a real option for them to decrease this work.

This functionality is an important unlock for GitLab to help smooth things for our new customers with material opportunity for our Sales team. Every day matters in terms of delivering features here.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AJenksin%20Importer)
- [Overall Vision](/direction/cicd)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Currently we have documentation work ([gitlab#37032](https://gitlab.com/gitlab-org/gitlab/issues/37032)) in progress to illustrate how to use the `jenkins-runner` project to embed your existing Jenkins pipelines inside of GitLab.

## Maturity Plan

TBD

## Competitive Landscape

There are existing importers out there of various capability and reliability. Atlassian has [documentation](https://confluence.atlassian.com/bamboo/importing-data-from-jenkins-317949257.html) on their solution, as well as a [video demo](https://www.youtube.com/watch?v=9SGWRWcJTyA). Bamboo also offers [documentation on their solution](https://confluence.atlassian.com/bamboo/importing-data-from-jenkins-317949257.html).

There is also some prior art of [conversions in the opposite direction](https://jenkins.io/blog/2018/04/25/configuring-jenkins-pipeline-with-yaml-file/) which may be illustrative.

## Top Customer Success/Sales Issue(s)

We haven't yet seen a consensus on the sales/CS side on which of the three pillars (listed below in your vision section) are most important to deliver first, but conversations are ongoing. In a general sense, all are important unlocks.

## Top Customer Issue(s)

There are a few places where we can improve our [customer-facing migration documentation](https://docs.gitlab.com/ee/ci/jenkins/index.html):

- [gitlab#208274](https://gitlab.com/gitlab-org/gitlab/issues/208274) takes the learnings from our field support team and adds them to the docs.
- [gitlab#208273](https://gitlab.com/gitlab-org/gitlab/issues/208273) will add a step by step blog post for actually performing an example migration
- [gitlab#208272](https://gitlab.com/gitlab-org/gitlab/issues/208272) will make a similar video for those who prefer a video to a blog post.

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

There are three pillars in our vision for this category representing the kinds of Jenkinsfiles that need to be imported or ways we can execute them:

- [gitlab#208277](https://gitlab.com/gitlab-org/gitlab/issues/208277): Wrapper for Jenkins execution inside of GitLab
- [gitlab#208276](https://gitlab.com/gitlab-org/gitlab/issues/208276): Importer for declarative and imperative Jenkins configuration
- [gitlab#208275](https://gitlab.com/gitlab-org/gitlab/issues/208275): Importer for scripted Jenkins configuration

We are also planning to research the most popular plugins ([gitlab#208720](https://gitlab.com/gitlab-org/gitlab/issues/208270))and ensure we have built-in features to provide a clear migration path.
